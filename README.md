# Learn Go

This document contains resources (mostly written) that you can learn Go from.

## Generics

* Read [An Introduction To Generics](https://go.dev/blog/intro-generics).
* Read the corresponding [proposal document](https://go.googlesource.com/proposal/+/HEAD/design/43651-type-parameters.md).
